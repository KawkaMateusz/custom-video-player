'use strict';

(function () {
    var _this = this;

    var mediaPlayer = document.getElementById('media-player'),
        playPauseBtn = document.getElementById('play-pause-btn'),
        stopBtn = document.getElementById('stop-btn'),
        muteBtn = document.getElementById('mute-btn'),
        progressBar = document.getElementById('progress-bar'),
        forwardBtn = document.getElementById('forward-btn'),
        backBtn = document.getElementById('back-btn'),
        currentTimeInfo = document.getElementById('current-time-view'),
        durationTimeInfo = document.getElementById('duration-time-view'),
        fullscreenBtn = document.getElementById('fullscreen'),
        volumeInput = document.getElementById('volume-input');

    mediaPlayer.addEventListener('onload', function () {
        return showDurationTime();
    });
    playPauseBtn.addEventListener('click', function () {
        return tooglePlayPause();
    });
    stopBtn.addEventListener('click', function () {
        return stopVideo();
    });
    muteBtn.addEventListener('click', function () {
        return muteVideo();
    });
    mediaPlayer.addEventListener('timeupdate', function () {
        return updateProgressBar();
    });
    backBtn.addEventListener('click', function () {
        return moveByBtn(-5);
    });
    forwardBtn.addEventListener('click', function () {
        return moveByBtn(5);
    });
    fullscreenBtn.addEventListener('click', function () {
        return fullScreenVideo();
    });
    volumeInput.addEventListener('input', function () {
        return changeVolume();
    });
    progressBar.addEventListener("click", shift);
    mediaPlayer.controls = false;
    var play = false;

    var tooglePlayPause = function tooglePlayPause() {
        if (mediaPlayer.paused) {
            mediaPlayer.play();
            play = true;
            playPauseBtn.classList.remove('fa-play-circle');
            playPauseBtn.classList.add('fa-pause-circle');
        } else {
            mediaPlayer.pause();
            play = false;
            playPauseBtn.classList.remove('fa-pause-circle');
            playPauseBtn.classList.add('fa-play-circle');
        }
    };

    var stopVideo = function stopVideo() {
        mediaPlayer.pause();
        mediaPlayer.currentTime = 0;
        if (playPauseBtn.classList.contains('fa-play-circle')) {
            playPauseBtn.classList.remove('fa-play-circle');
            playPauseBtn.classList.add('fa-pause-circle');
        } else {
            playPauseBtn.classList.remove('fa-pause-circle');
            playPauseBtn.classList.add('fa-play-circle');
        }
    };

    var muteVideo = function muteVideo() {
        if (mediaPlayer.muted) {
            mediaPlayer.muted = false;
            muteBtn.classList.remove('active');
        } else {
            mediaPlayer.muted = true;
            muteBtn.classList.add('active');
        }
    };

    var scrollVideo = function scrollVideo(e) {
        var z = _this.offsetLeft;
        var x = e.pageX - _this.offsetLeft;
        console.log(x);
        console.log(z);
    };

    var moveByBtn = function moveByBtn(v) {
        progressBar.value += v;
        mediaPlayer.currentTime += mediaPlayer.duration / 100 * v;
    };

    var updateProgressBar = function updateProgressBar() {
        var percentage = Math.floor(100 / mediaPlayer.duration * mediaPlayer.currentTime);
        progressBar.value = percentage;
    };

    var showCurrentTime = function showCurrentTime() {
        var time = mediaPlayer.currentTime;
        console.log(time);
    };

    var showDurationTime = function showDurationTime() {
        var duration = mediaPlayer.duration;
        var y = duration.toFixed(2);
        console.log(y);
        durationTimeInfo.innerHTML += duration;
    };

    var fullScreenVideo = function fullScreenVideo() {
        if (mediaPlayer.requestFullscreen) {
            mediaPlayer.requestFullscreen();
        } else if (mediaPlayer.mozRequestFullScreen) {
            mediaPlayer.mozRequestFullScreen(); // Firefox
        } else if (mediaPlayer.webkitRequestFullscreen) {
            mediaPlayer.webkitRequestFullscreen(); // Chrome and Safari
        }
    };

    var changeVolume = function changeVolume() {
        var value = volumeInput.value / 100;
        mediaPlayer.volume = value;
        console.log(value);
    };

    var shift = function shift(el) {
        var percent = el.offsetX / this.offsetWidth;
        mediaPlayer.currentTime = percent * mediaPlayer.duration;
        progressBar.value = percent / 100;
    };
})();