var gulp = require('gulp');
var pug = require('gulp-pug');
var babel = require('gulp-babel');
var autoprefixer = require('gulp-autoprefixer');
 

gulp.task('build', ['views', 'babel', 'autoprefixer']);

gulp.task('views', function buildHTML() {
  return gulp.src('src/index.pug')
  .pipe(pug({
    
  })).pipe(gulp.dest('dist'))
});

gulp.task('babel', function () {
  gulp.src('src/scripts/main.js')
      .pipe(babel({
          presets: ['env']
      }))
      .pipe(gulp.dest('dist'))
});

gulp.task('autoprefixer', function () {
    gulp.src('src/styles/main.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
});



