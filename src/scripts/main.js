(function() {

    const  mediaPlayer = document.getElementById('media-player'),
    playPauseBtn = document.getElementById('play-pause-btn'),
    stopBtn = document.getElementById('stop-btn'),
    muteBtn = document.getElementById('mute-btn'),
    progressBar = document.getElementById('progress-bar'),
    forwardBtn = document.getElementById('forward-btn'),
    backBtn = document.getElementById('back-btn'),
    currentTimeInfo = document.getElementById('current-time-view'),
    durationTimeInfo = document.getElementById('duration-time-view'),
    fullscreenBtn = document.getElementById('fullscreen'),
    volumeInput = document.getElementById('volume-input');



    mediaPlayer.addEventListener('onload', () => showDurationTime())
    playPauseBtn.addEventListener('click', () => tooglePlayPause());
    stopBtn.addEventListener('click', () => stopVideo());
    muteBtn.addEventListener('click', () => muteVideo());
    mediaPlayer.addEventListener('timeupdate', () => updateProgressBar())
    backBtn.addEventListener('click', () => moveByBtn(-5))
    forwardBtn.addEventListener('click', () => moveByBtn(5))
    fullscreenBtn.addEventListener('click', () => fullScreenVideo())
    volumeInput.addEventListener('input', () => changeVolume())
    progressBar.addEventListener("click", shift);
    mediaPlayer.controls = false;
    let play = false;
        

    const tooglePlayPause = () => {
        if(mediaPlayer.paused) {
            mediaPlayer.play();
            play = true
            playPauseBtn.classList.remove('fa-play-circle');
            playPauseBtn.classList.add('fa-pause-circle');
        } else {
            mediaPlayer.pause();
            play = false;
            playPauseBtn.classList.remove('fa-pause-circle');
            playPauseBtn.classList.add('fa-play-circle');
    
        }
    }
    
    const stopVideo = () => {
        mediaPlayer.pause();
        mediaPlayer.currentTime = 0;
        if(playPauseBtn.classList.contains('fa-play-circle')) {
            playPauseBtn.classList.remove('fa-play-circle');
            playPauseBtn.classList.add('fa-pause-circle');
        } else {
            playPauseBtn.classList.remove('fa-pause-circle');
            playPauseBtn.classList.add('fa-play-circle');
        }
    }
    
    const muteVideo = () => {
        if(mediaPlayer.muted) {
            mediaPlayer.muted = false;
            muteBtn.classList.remove('active');
        } else {
            mediaPlayer.muted = true;
            muteBtn.classList.add('active');
        }
    }
    
    const scrollVideo = (e) => {
        let z = this.offsetLeft
        let x = e.pageX - this.offsetLeft
        console.log(x)
        console.log(z)
    }
    
    const moveByBtn = (v) => {
        progressBar.value += v;
        mediaPlayer.currentTime += (mediaPlayer.duration / 100) * v
    }
    
    const updateProgressBar = () => {
        let percentage = Math.floor((100 / mediaPlayer.duration) * mediaPlayer.currentTime)
        progressBar.value = percentage;
    }
    
    const showCurrentTime = () => {
        let time = mediaPlayer.currentTime;
        console.log(time)
    }
    
    const showDurationTime = () => {
        let duration = mediaPlayer.duration;
        let y = duration.toFixed(2);
        console.log(y)
        durationTimeInfo.innerHTML += duration
    }

    const fullScreenVideo = () => {
        if (mediaPlayer.requestFullscreen) {
            mediaPlayer.requestFullscreen();
        } else if (mediaPlayer.mozRequestFullScreen) {
            mediaPlayer.mozRequestFullScreen(); // Firefox
        } else if (mediaPlayer.webkitRequestFullscreen) {
            mediaPlayer.webkitRequestFullscreen(); // Chrome and Safari
        }
    }

    const changeVolume = () => {
        let value = volumeInput.value / 100;
        mediaPlayer.volume = value;
        console.log(value)
    }

    const shift = function(el) {
        let percent = el.offsetX / this.offsetWidth;
        mediaPlayer.currentTime = percent * mediaPlayer.duration;
        progressBar.value = percent / 100;
    }




})();
   